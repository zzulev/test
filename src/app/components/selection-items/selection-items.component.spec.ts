import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectionItemsComponent } from './selection-items.component';

describe('SelectionItemsComponent', () => {
  let component: SelectionItemsComponent;
  let fixture: ComponentFixture<SelectionItemsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectionItemsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectionItemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
