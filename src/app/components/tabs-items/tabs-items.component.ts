import { Component, Input } from '@angular/core';

import { Tab } from '../../models/tab.model';

@Component({
  selector: 'app-tabs-items',
  templateUrl: './tabs-items.component.html',
  styleUrls: ['./tabs-items.component.scss']
})
export class TabsItemsComponent {
  @Input() tabs: Tab[];

}
