import { Component, Input } from '@angular/core';

import { Tab } from '../../models/tab.model';

@Component({
  selector: 'app-tabs-item',
  templateUrl: './tabs-item.component.html',
  styleUrls: ['./tabs-item.component.scss']
})
export class TabsItemComponent {
  @Input() tab: Tab;

}
