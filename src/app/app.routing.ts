import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SelectionComponent } from './containers/selection/selection.component';
import { TabsComponent } from './containers/tabs/tabs.component';

const routes: Routes = [
  { path: '', redirectTo: 'selection', pathMatch: 'full' },
  { path: 'selection', component: SelectionComponent },
  { path: 'tabs', component: TabsComponent }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes);