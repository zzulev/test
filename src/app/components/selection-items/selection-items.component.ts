import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Tab } from '../../models/tab.model';

@Component({
  selector: 'app-selection-items',
  templateUrl: './selection-items.component.html',
  styleUrls: ['./selection-items.component.scss']
})
export class SelectionItemsComponent {
  @Input() tabs: Tab[];
  @Output() toggleSelect = new EventEmitter<any>();

}
