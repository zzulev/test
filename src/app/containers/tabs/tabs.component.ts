import { Component, OnInit } from '@angular/core';
import { TabsService } from '../../services/tabs.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.scss']
})
export class TabsComponent implements OnInit {

  constructor(
    private $tabs: TabsService,
    private router: Router
  ) { }

  ngOnInit() {
    if (!this.$tabs.selectedTabs) {
      this.router.navigate(['/selection'])
    }
  }

}
