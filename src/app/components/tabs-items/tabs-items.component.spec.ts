import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabsItemsComponent } from './tabs-items.component';

describe('TabsItemsComponent', () => {
  let component: TabsItemsComponent;
  let fixture: ComponentFixture<TabsItemsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabsItemsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabsItemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
