import { Component, OnInit } from '@angular/core';

import { TabsService } from './../../services/tabs.service';
import { Tab } from '../../models/tab.model';

@Component({
  selector: 'app-selection',
  templateUrl: './selection.component.html',
  styleUrls: ['./selection.component.scss']
})
export class SelectionComponent implements OnInit {
  tabs: Tab[];

  constructor(
    private $tabs: TabsService,
  ) { }

  ngOnInit() {
    this.$tabs.selectedTabs = [];
    this.$tabs.list().subscribe(tabs => this.tabs = tabs);
  }

  onToggleSelect(tab: Tab) {
    if (tab.selected) {
      this.$tabs.selectedTabs.push(tab);
    } else {
      const index = this.$tabs.selectedTabs.indexOf(tab);
      this.$tabs.selectedTabs.splice(index, 1);
    }
  }

}
