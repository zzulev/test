import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MdTabsModule, MdCheckboxModule, MdButtonModule } from '@angular/material';

import { AppComponent } from './app.component';
import { routing } from './app.routing';
import { SelectionComponent } from './containers/selection/selection.component';
import { TabsComponent } from './containers/tabs/tabs.component';
import { TabsService } from './services/tabs.service';
import { SelectionItemsComponent } from './components/selection-items/selection-items.component';
import { SelectionItemComponent } from './components/selection-item/selection-item.component';
import { TabsItemsComponent } from './components/tabs-items/tabs-items.component';
import { TabsItemComponent } from './components/tabs-item/tabs-item.component';

@NgModule({
  declarations: [
    AppComponent,
    SelectionComponent,
    TabsComponent,
    SelectionItemsComponent,
    SelectionItemComponent,
    TabsItemsComponent,
    TabsItemComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    BrowserAnimationsModule,
    MdTabsModule,
    MdCheckboxModule,
    MdButtonModule,
    routing,
  ],
  providers: [
    TabsService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
