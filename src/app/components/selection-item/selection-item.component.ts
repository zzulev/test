import { Component, Input, Output, EventEmitter } from '@angular/core';

import { Tab } from '../../models/tab.model';

@Component({
  selector: 'app-selection-item',
  templateUrl: './selection-item.component.html',
  styleUrls: ['./selection-item.component.scss']
})
export class SelectionItemComponent {
  @Input() tab: Tab;
  @Output() toggleSelect = new EventEmitter<any>();

}
