import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { Tab } from '../models/tab.model';

@Injectable()
export class TabsService {
  selectedTabs: Tab[];

  constructor() { }

  list(): Observable<Tab[]> {
    return Observable.create(observer => {
      observer.next([
        {
          id: 1,
          label: 'Первый таб',
          content: 'Контент первого таба',
        },
        {
          id: 2,
          label: 'Второй таб',
          content: 'Контент второго таба',
        },
        {
          id: 3,
          label: 'Третий таб',
          content: 'Контент третьего таба',
        },
        {
          id: 4,
          label: 'Четвертый таб',
          content: 'Контент четвертого таба',
        },
        {
          id: 5,
          label: 'Пятый таб',
          content: 'Контент пятого таба',
        }
      ])
    })
  }

}
