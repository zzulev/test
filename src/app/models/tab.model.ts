export interface Tab {
  id: number;
  label: string;
  content: string;
  selected?: boolean;
}
